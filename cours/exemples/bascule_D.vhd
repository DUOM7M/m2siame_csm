LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY bascule_D IS
	PORT (	D, clk	: IN STD_LOGIC;
			Q		: INOUT STD_LOGIC;
			nQ		: OUT STD_LOGIC);
END ENTITY bascule_D;



ARCHITECTURE compD OF bascule_D IS
BEGIN
	nQ <= NOT Q;
	PROCESS(clk)
	BEGIN
		IF RISING_EDGE(clk) THEN
		-- Equivalent de : IF clk'EVENT AND clk='1' THEN
		-- Lui-même équivalent de clk='1' puisque clk est dans la liste de sensibilité
			Q <= D;
		END IF;
	END PROCESS;
END ARCHITECTURE compD;
