LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY compteur IS
	PORT (	clk		: IN STD_LOGIC;
			rst		: IN STD_LOGIC;
			dataOut	: OUT STD_LOGIC_VECTOR(3 DOWNTO 0));
END ENTITY compteur;



ARCHITECTURE rtl OF compteur IS
	SIGNAL countRegister : UNSIGNED(3 DOWNTO 0);
BEGIN
	dataOut <= STD_LOGIC_VECTOR(countRegister);
	PROCESS(clk, rst)
	BEGIN
		IF rst = '1' THEN
			countRegister <= TO_UNSIGNED(0, countRegister'LENGTH);
		ELSIF RISING_EDGE(clk) THEN
			countRegister <= countRegister + 1;
		END IF;
	END PROCESS;
END ARCHITECTURE rtl;
