LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE IEEE.NUMERIC_STD.ALL;

ENTITY test IS
END ENTITY;

ARCHITECTURE arc OF test IS

	SIGNAL rst : STD_LOGIC := '0';
	SIGNAL clk : STD_LOGIC := '0';
	SIGNAL clk_en : STD_LOGIC := '1';
	SIGNAL dataOut	: STD_LOGIC_VECTOR(3 DOWNTO 0);

	CONSTANT periode : time := 10 ns;

	COMPONENT compteur IS
		PORT (	clk		: IN STD_LOGIC;
				rst		: IN STD_LOGIC;
				dataOut	: OUT STD_LOGIC_VECTOR(3 DOWNTO 0));
	END COMPONENT;

BEGIN
	-- instanciation duc omposant
	cpt : ENTITY work.compteur PORT MAP (clk, rst, dataOut);
	-- Génération de l'horloge
	clk <= (NOT clk) AND clk_en AFTER periode/2;
	-- Un petit reset au milieu...
	rst <= '1' AFTER 32 ns, '0' AFTER 43 ns, '1' AFTER 297 ns, '0' AFTER 312 ns;
	-- clk_en sert à arrêter le signal d'horloge lorsque la simulation est considérée comme terminée
	clk_en <= '0' AFTER 600 ns;

END ARCHITECTURE;
