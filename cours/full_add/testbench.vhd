LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE IEEE.NUMERIC_STD.ALL;

ENTITY test IS
END ENTITY;

ARCHITECTURE arc OF test IS
    COMPONENT full_add IS
        PORT (	A, B, Cin	: IN STD_LOGIC;
                S, Cout		: OUT STD_LOGIC);
    END COMPONENT;


	SIGNAL A, B, Cin, S, Cout : STD_LOGIC := '0';

BEGIN
	-- test du circuit full_add
	adder : full_add PORT MAP (A, B, Cin, S, Cout);

	PROCESS 
		TYPE TableDeVerite IS ARRAY(0 TO 7) OF STD_LOGIC_VECTOR(2 DOWNTO 0);
		constant TdV : TableDeVerite :=("000", "001", "010", "011", "100", "101", "110", "111");
	BEGIN
		FOR I IN TdV'RANGE LOOP
			A <= TdV(I)(2);
			B <= TdV(I)(1);
			Cin <= TdV(I)(0);
			WAIT FOR 5 ns;
		END LOOP;
		WAIT;
	END PROCESS;

END ARCHITECTURE;
