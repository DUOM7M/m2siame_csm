-- Parity generator entity

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY full_add IS
        PORT (	A, B, Cin	: IN STD_LOGIC;
                S, Cout		: OUT STD_LOGIC);
END ENTITY full_add;


ARCHITECTURE fdd_full_add OF full_add IS
BEGIN
    S <= A XOR B XOR Cin;
    Cout <= (A AND B) OR (Cin AND (A XOR B));
END ARCHITECTURE fdd_full_add;
