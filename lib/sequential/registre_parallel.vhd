-- Definition des librairies
library IEEE;

-- Definition des portee d'utilisation
use IEEE.std_logic_1164.all;
--use IEEE.std_logic_arith.all;

use IEEE.std_logic_unsigned.all;
USE IEEE.numeric_std.ALL; 
USE IEEE.math_real.ALL;


ENTITY parallel_register IS

	-- definition des parametres generiques
	generic	(
		-- largeur du bus d'entree par default
		DATA_WIDTH	: integer := 32);

        port (	
		-- data
		W: IN STD_LOGIC; 
		D_IN:	IN STD_LOGIC_VECTOR(DATA_WIDTH-1 downto 0); 
		D_OUT:	OUT STD_LOGIC_VECTOR(DATA_WIDTH-1 downto 0) );

END ENTITY parallel_register;

ARCHITECTURE bhv_parallel_register OF parallel_register IS
BEGIN
	PROCESS(W)
	BEGIN
		if rising_edge(W) then
			D_OUT <= D_IN;	
		end if;
	END PROCESS;
END ARCHITECTURE bhv_parallel_register;


