
-- registerFile entity

-- Definition des librairies
library IEEE;

-- Definition des portee d'utilisation
use IEEE.std_logic_1164.all;
--use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
USE IEEE.numeric_std.ALL; 
USE IEEE.math_real.ALL; 

ENTITY decoder IS

	-- definition des parametres generiques
	generic	(
		-- largeur du bus d'entree par default
		IN_WIDTH	: integer := 5 );

        port (	
		-- data
		D_IN:	IN STD_LOGIC_VECTOR(IN_WIDTH-1 downto 0);
		D_OUT:	OUT STD_LOGIC_VECTOR((2**IN_WIDTH)-1 downto 0) );

END ENTITY decoder;

ARCHITECTURE fdd_decoder OF decoder IS
BEGIN
	D_OUT <= ( to_integer(unsigned(D_IN)) => '1', OTHERS => '0');
END ARCHITECTURE fdd_decoder;

ARCHITECTURE bhv_decoder OF decoder IS
BEGIN
	PROCESS(D_IN)
	BEGIN
		D_OUT <= ( to_integer(unsigned(D_IN)) => '1', OTHERS => '0');	
	END PROCESS;
END ARCHITECTURE bhv_decoder;