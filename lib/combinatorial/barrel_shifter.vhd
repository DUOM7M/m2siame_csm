-- Definition des librairies
library IEEE;
library cpupackage;
-- Definition des portee d'utilisation
use IEEE.std_logic_1164.all;
--use IEEE.std_logic_arith.all;

use IEEE.std_logic_unsigned.all;
USE IEEE.numeric_std.ALL; 
USE IEEE.math_real.ALL;

use cpupackage.cpu_package.all;


ENTITY barrel_shifter IS

	-- definition des parametres generiques
	generic	(
		-- largeur du bus d'entree par default
		shift_size	: integer := 5;
		data_width	: integer := 32
	);

        port (	
		-- data
		data_in: 				IN STD_LOGIC_VECTOR(data_width-1 downto 0); 
		shift_amount:				IN STD_LOGIC_VECTOR(shift_size-1 downto 0);
		leftRight, logicArith, shiftRotate:	IN std_logic;
		data_out:				OUT STD_LOGIC_VECTOR(data_width-1 downto 0) 
	);

END ENTITY barrel_shifter;

ARCHITECTURE fdd_barrel_shifter OF barrel_shifter IS
	signal mux_in : std_logic_vector(2 downto 0);
BEGIN
	mux_in(0) = leftRight;
	mux_in(1) = logicArith;
	mux_in(2) = shiftRotate;

	with mux_in select
		 data_out <= when '000',		-- arith right
		 data_out <= when '001',		-- arith left
		 data_out <= when '010',		-- logic right
		 data_out <= when '011',		-- logic right
		 data_out <= when '100' | '110',	-- rotate right
		 data_out <= when '101' | '111';	-- rotate right


END ARCHITECTURE fdd_multiplexer;r
