
-- Definition des librairies
library IEEE;
-- Definition des portee d'utilisation
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

use IEEE.std_logic_unsigned.all;
USE IEEE.numeric_std.ALL; 
USE IEEE.math_real.ALL;

package cpu_package is
	type MUX_ARRAY is array (integer range <>) of std_logic_vector;

-- ===============================================================
-- DEFINITION DE FONCTIONS/PROCEDURES
-- ===============================================================
	-- fonction log2
	--		calcule le logarithme base2 d'un entier naturel, ou plus exactement
	--		renvoie le nombre de bits necessaire pour coder un entier naturel I
	function log2 (I: in natural) return natural;

end cpu_package;

package body cpu_package is

-- fonction log2
function log2 (I: in natural) return natural is
	variable octet : std_logic_vector(7 downto 0); 
	variable found : boolean := false;
	variable res : natural := 0;
	variable indice: integer := 0;
begin
	octet := conv_std_logic_vector(I, 8);
	
	for i in octet'low to octet'high loop
            report "octet(" & integer'image(i) & ") value is" & std_logic'image(octet(i));
       	end loop;

	--report "The value of 'octet' is " & integer'image(octet);

	for indice in octet'range loop
		if ( octet(indice) = '1' ) then 
			if (not found) then
				found := true;
				res := indice;
			else
				res := res + 1;
			end if;
		end if;
	end loop;
	return res;
end log2;

end cpu_package;