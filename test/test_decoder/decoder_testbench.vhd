LIBRARY IEEE;
library WORK;
library combinatorialtools;
USE IEEE.STD_LOGIC_1164.all;
USE IEEE.NUMERIC_STD.ALL;

ENTITY decoder_tb IS
END ENTITY;

architecture arc OF decoder_tb IS
	constant inwidth: positive:=5;	

	signal indata : STD_LOGIC_VECTOR(inwidth-1 downto 0);
	signal outdata: STD_LOGIC_VECTOR((2**inwidth)-1 downto 0);


BEGIN
	-- instanciation du composant
	dec :	entity combinatorialtools.decoder(fdd_decoder)
		generic map (inwidth)
		port map (indata, outdata);		

	--indata <= std_logic_vector(to_unsigned(2, inwidth));

-- ca Ne marche PAS
--	test_cases_loop: for i in 0 to (2**inwidth)-1 generate
--		indata <= std_logic_vector(to_unsigned(i, inwidth)) after (1000ns*(i+1));
--	end generate;
	
	--indata <= "00000", "00001" after 80 ns, "00010" after 160 ns, "00011" after 240 ns;

	test_cases: process
	begin
		for i in 0 to (2**inwidth)-1 loop
			indata <= std_logic_vector(to_unsigned(i, inwidth));
			wait for 5ns;
		end loop;
		wait;
	end process test_cases;

END arc;
