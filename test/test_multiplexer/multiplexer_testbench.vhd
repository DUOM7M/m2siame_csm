LIBRARY IEEE;
library WORK;
library combinatorialtools;
USE IEEE.STD_LOGIC_1164.all;
USE IEEE.NUMERIC_STD.ALL;
use combinatorialtools.mux_types.all;

ENTITY multiplexer_tb IS
END multiplexer_tb;

architecture arc OF multiplexer_tb IS
	constant datawidth: positive:=32;	
	constant addrwidth: positive:=2;

	signal addrin : STD_LOGIC_VECTOR(addrwidth-1 downto 0);
	signal indata : MUX_ARRAY(0 to ((2**addrwidth)-1))(datawidth-1 downto 0);
	signal outdata: STD_LOGIC_VECTOR(datawidth-1 downto 0);

BEGIN
	-- instanciation du composant
	mux :	entity combinatorialtools.multiplexer(fdd_multiplexer)
		generic map (datawidth, addrwidth)
		port map (addrin, indata, outdata);		

	--indata <= std_logic_vector(to_unsigned(2, inwidth));

-- ca Ne marche PAS
--	test_cases_loop: for i in 0 to (2**inwidth)-1 generate
--		indata <= std_logic_vector(to_unsigned(i, inwidth)) after (1000ns*(i+1));
--	end generate;
	
	--indata <= "00000", "00001" after 80 ns, "00010" after 160 ns, "00011" after 240 ns;

	test_cases: process
	begin
		for i in 0 to (2**addrwidth)-1 loop
			addrin <= std_logic_vector(to_unsigned(i, addrwidth));
			indata(i) <= std_logic_vector(to_unsigned(i, datawidth));
			wait for 5ns;
		end loop;
		wait;
	end process test_cases;

END arc;
