LIBRARY IEEE;
library WORK;
library sequentialtools;
library combinatorialtools;
library cpupackage;
use cpupackage.cpu_package.all;
USE IEEE.STD_LOGIC_1164.all;
USE IEEE.NUMERIC_STD.ALL;

ENTITY register_file_tb IS
END ENTITY;

architecture arc OF register_file_tb IS
	constant datawidth: positive:=32;
	constant regs_nbre: positive:=32;
	constant clkpulse : Time := 5 ns; -- 1/2 periode horloge

	signal clk, w : std_logic;
	signal indata, outdata0, outdata1 : STD_LOGIC_VECTOR(datawidth-1 downto 0);
	signal addr_in, addr_out0, addr_out1 : STD_LOGIC_VECTOR(log2(regs_nbre)-1 downto 0);
BEGIN
	-- instanciation du composant
	reg_file :	entity sequentialtools.register_file(struct_register_file)
			generic map (datawidth, regs_nbre)
			port map (w, clk, indata, outdata0, outdata1, addr_in, addr_out0, addr_out1);		

	--indata <= std_logic_vector(to_unsigned(2, inwidth));

-- ca Ne marche PAS
--	test_cases_loop: for i in 0 to (2**inwidth)-1 generate
--		indata <= std_logic_vector(to_unsigned(i, inwidth)) after (1000ns*(i+1));
--	end generate;
	
	--indata <= "00000", "00001" after 80 ns, "00010" after 160 ns, "00011" after 240 ns;

	-- definition de l'horloge
	p_clk: process
	begin
		clk <= '1';
		wait for clkpulse;
		clk <= '0';
		wait for clkpulse;
	end process p_clk;

	w <= '1';

	test_cases: process --(clk)
--		variable cpt : integer := 0;
	begin
--		if falling_edge(clk) then
--			cpt := cpt + 1;
--			addr_in <= std_logic_vector(to_unsigned(i, log2(regs_nbre)));
--			indata <= std_logic_vector(to_unsigned(cpt, datawidth));
--		end if;
		for i in 1 to regs_nbre-1 loop
--			clk <= '0';
			wait until falling_edge(clk);
			addr_in <= std_logic_vector(to_unsigned(i, addr_in'length));
			addr_out0 <= std_logic_vector(to_unsigned(i-1, addr_out0'length));
			addr_out1 <= std_logic_vector(to_unsigned(i, addr_out1'length));
			indata <= std_logic_vector(to_unsigned(i+5, indata'length));
--			wait for clkpulse;
--			clk <= '1';
--			wait for clkpulse;
		end loop;
		wait;
	end process test_cases;

END arc;
