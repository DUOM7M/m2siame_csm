LIBRARY IEEE;
library WORK;
library sequentialtools;
USE IEEE.STD_LOGIC_1164.all;
USE IEEE.NUMERIC_STD.ALL;

ENTITY parallel_register_tb IS
END ENTITY;

architecture arc OF parallel_register_tb IS
	constant datawidth: positive:=32;	
	constant clkpulse : Time := 5 ns; -- 1/2 periode horloge

	signal clk : std_logic;
	signal indata : STD_LOGIC_VECTOR(datawidth-1 downto 0);
	signal outdata: STD_LOGIC_VECTOR(datawidth-1 downto 0);


BEGIN
	-- instanciation du composant
	dec :	entity sequentialtools.parallel_register(bhv_parallel_register)
		generic map (datawidth)
		port map (clk, indata, outdata);		

	--indata <= std_logic_vector(to_unsigned(2, inwidth));

-- ca Ne marche PAS
--	test_cases_loop: for i in 0 to (2**inwidth)-1 generate
--		indata <= std_logic_vector(to_unsigned(i, inwidth)) after (1000ns*(i+1));
--	end generate;
	
	--indata <= "00000", "00001" after 80 ns, "00010" after 160 ns, "00011" after 240 ns;

	-- definition de l'horloge
	p_clk: process
	begin
		clk <= '1';
		wait for clkpulse;
		clk <= '0';
		wait for clkpulse;
	end process p_clk;


	test_cases: process (clk)
		variable cpt : integer := 0;
	begin
		if falling_edge(clk) then
			cpt := cpt + 1;
			indata <= std_logic_vector(to_unsigned(cpt, datawidth));
	
		end if;
	end process test_cases;

END arc;
